// API

/*
placeholder API: https://jsonplaceholder.typicode.com/posts 

*/

fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((data) => showPosts(data));

const showPosts = (posts) => {
	let postEntries = '';

	
	posts.forEach((post)=>{
	
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// ?add post data

document.querySelector('#form-add-post').addEventListener('submit', (e) => {e.preventDefault();

fetch('https://jsonplaceholder.typicode.com/posts', {
	method: "POST",

	body: JSON.stringify({
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value,
		userId: 1,
	}),
	headers: {'Content-type': 'application/json; charset=UTF-8' }
})
	.then((response)=> response.json())
	.then((data) =>{
	console.log(data);
	alert('Successfully added');

	// lets input be empty after submit button is triggered
	document.querySelector('#txt-title').value=null;

	document.querySelector('#txt-body').value=null;


	})

});

// MINI ACTIVITY

// edit post

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// Update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {e.preventDefault();

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: "PUT",

	body: JSON.stringify({
		title: document.querySelector("#txt-edit-title").value,
		body: document.querySelector("#txt-edit-body").value,
		userId: 1,
	}),
	headers: {'Content-type': 'application/json; charset=UTF-8' }
})
	.then((response)=> response.json())
	.then((data) =>{
	console.log(data);
	alert('Successfully updated');

	document.querySelector(`#post-title-${data.id}`).innerHTML = data.title;
    document.querySelector(`#post-body-${data.id}`).innerHTML = data.body;

	// lets input be empty after submit button is triggered
	document.querySelector('#txt-edit-title').value=null;

	document.querySelector('#txt-edit-body').value=null;
	document.querySelector('#txt-edit-title').value=null;

	document.querySelector('#txt-edit-id').value=null;

	// sets button disabled after update button has been triggered

	document.querySelector('#btn-submit-update').setAttribute('disabled', true);

	})

});

// activity

const deletePost=(id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
	method: "DELETE"});
	document.querySelector(post).remove();
}
